package com.example.fela;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.fela.CameraApi1.Preview;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by karl on 2015-05-09.
 *
 */
public class FragmentCamera extends Fragment {

    private FragmentCameraActivityInterface activityInterface;
    private LocationManager locationManager;    // location manager
    private LocationListener locationListener;  // location listener
    private CameraApi1 camera;
    private MainActivity activity;

    // camera api 2 (api > 21)
    // yet to be implemented

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (MainActivity)activity;
        try {
            activityInterface = (FragmentCameraActivityInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ExampleFragmentCallbackInterface ");
        }
    }

    public interface FragmentCameraActivityInterface {
        public int getCurrentPage();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // check if location services is on
        if (!isLocationEnabled(getActivity())) {
            toast("Enable location services to use fela", "long");
            // redirect the user to the location services settings (to turn them on)
            Intent i = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(i);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i("karl", "FragmentCamera onCreateView()");
        View view = null;

        if(camera == null)
            camera = new CameraApi1(getActivity());

        if(camera.hasCamera()) { // does the device support a camera

            view = inflater.inflate(R.layout.fragment_camera, container, false);
            LinearLayout previewLayout = (LinearLayout) view.findViewById(R.id.camera_preview);

            // location manager required to call requestLocationUpdates
            locationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
            // location listener to update users current position
            locationListener = new FelaLocationListener();
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP
                    && camera.getCameraIdFacingBack() >= 0) { // is between api 1 - 21 (camera api 1) & has back facing camera

                // fixes surface view, then starts the preview
                Preview preview = camera.getPreviewInstance(camera.getCameraIdFacingBack());
                previewLayout.addView(preview);
                camera.setPictureCallback(new CameraPictureCallback());
//                preview = new Preview(getActivity(), this);
//                previewLayout.addView(preview);


                // setting up take pic button
                ImageButton takePicBtn = (ImageButton) view.findViewById(R.id.ibtn_take_pic);
                takePicBtn.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                                                // must have location services enabled to take a picture
                                if (!isLocationEnabled(getActivity())) {
                                    toast("Enable location services to use fela", "long");
                                    // redirect the user to the location services settings (to turn them on)
                                    Intent i = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    startActivity(i);
                                } else { // location services is on
                                    // request location update for the immediate picture
                                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, locationListener);
                                    // get an image from the camera and send it to the callback
                                    camera.takePic();
                                }
                            }
                        }
                );
            } else { // is api 21 or over (camera api 2)
                // yet to be implemented, use new camera api 2 instead, put apiPre21 prefix in front of methods to split them up, or reuse same methods but include camera api2 changes (if it has the same workflow)
            }
        } else { // the device has no camera
            // displays view with message stating no support for camera
            view = inflater.inflate(R.layout.fragment_no_camera, container, false);
        }
        return view;
    }

    @Override
    public void onResume() {
        Log.i("camera_api1", "onResume() thread: " + Thread.currentThread().getName() + " id: " + Thread.currentThread().getId());
        // start the camera and preview as this app will be using it
        camera.startPreview();
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.i("camera_api1", "onPause() thread: " + Thread.currentThread().getName() + " id: " + Thread.currentThread().getId());
        // stop preview and release camera so other apps can utilize the camera
        camera.releaseCamera();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // quit otherwise it will continue (waste)
        camera.quit();
    }

    protected void fragmentVisible() {
//        camera.startPreview();
    }

    protected void fragmentNotVisible() {
//        camera.releaseCamera();
    }

    private class CameraPictureCallback implements PictureCallback {
        @Override
        public void onPictureTaken(final byte[] data, final Camera realCamera) {
            Log.i("camera_api1", "CameraPictureCallback.onPictureTaken(..) thread: " + Thread.currentThread().getName() + " id: " + Thread.currentThread().getId());
            realCamera.startPreview();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Log.i("camera_api1", "runner thread: " + Thread.currentThread().getName() + " id: " + Thread.currentThread().getId());
                    // preparation for rotation
//                    Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);





                    BitmapFactory.Options opt;

                    opt = new BitmapFactory.Options();
                    opt.inTempStorage = new byte[16 * 1024];
                    Parameters parameters = realCamera.getParameters();
                    Size size = parameters.getPictureSize();

                    int height11 = size.height;
                    int width11 = size.width;
                    float mb = (width11 * height11) / 1024000;

                    if (mb > 4f)
                        opt.inSampleSize = 4;
                    else if (mb > 3f)
                        opt.inSampleSize = 2;

                    //preview from camera
                    Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length,opt);





                    int rotation = ((WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
                    int degrees = 0;
                    // specifically for back facing camera
                    switch (rotation) {
                        case Surface.ROTATION_0:
                            degrees = 90;
                            break;
                        case Surface.ROTATION_90:
                            degrees = 0;
                            break;
                        case Surface.ROTATION_180:
                            degrees = 270;
                            break;
                        case Surface.ROTATION_270:
                            degrees = 180;
                            break;
                    }

                    // rotate image to correct rotation before saving
                    Matrix matrix = new Matrix();
                    matrix.postRotate(degrees);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPurgeable = true;
                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

                    // create new file to write bitmap to
                    final File pictureFile = camera.getOutputMediaFile("fela");
                    if (pictureFile == null) {
                        return;
                    }

                    try {
                        // write to the file
                        FileOutputStream fos = new FileOutputStream(pictureFile);
                        // save the bitmap to a file compressed as a JPEG with 100% compression rate
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                        // put gps location into image, [MUST BE AFTER bitmap.compress]
                        ExifInterface exif = new ExifInterface(pictureFile.getCanonicalPath());
                        List<Double> latLon = getCurrentLatLon();
                        Log.i("karl", "getPictureCallback lat " + latLon.get(0) + "  lon " + latLon.get(1));
                        GPS gps = new GPS();

//                        exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, gps.convert(latLon.get(0)));
//                        exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, gps.convert(latLon.get(0)));
//                        exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, gps.convert(latLon.get(1)));
//                        exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, gps.convert(latLon.get(1)));
                        exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, gps.convert(latLon.get(0)));
                        exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, gps.convert(latLon.get(0)));
                        exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, gps.convert(latLon.get(1)));
                        exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, gps.convert(latLon.get(1)));

                        Log.i("karl", "lat convert " + gps.convert(latLon.get(0)));
                        Log.i("karl", "lon convert " + gps.convert(latLon.get(1)));


                        exif.setAttribute(ExifInterface.TAG_GPS_ALTITUDE, getAltitude()+"");
                        exif.setAttribute(ExifInterface.TAG_GPS_ALTITUDE_REF, getAltitude()+"");
                        exif.saveAttributes();

                        Log.i("LATITUDE EXTRACTED", exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE));
                        Log.i("LONGITUDE EXTRACTED", exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE));

                        // finised the stream
                        fos.flush();
                        fos.close();

                        toast("Picture saved", "long");
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    // make the picture visible to the rest of the device
                    camera.galleryAddPic(pictureFile);
                    camera.releaseCamera();
                    activity.setPictureTaken(pictureFile);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            activity.setCurrentPage(1);
                        }
                    });

                }
            }).start(); // to start the camera, but we want to change the fragment
        }
    }

    /**
     * checks if the devices location services is on
     * @param context
     * @return
     */
    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (SettingNotFoundException e) {
                e.printStackTrace();
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    /**
     * gets the current users location and returns it
     * @return latitude and longitude coordinates in a double list
     */
    public List<Double> getCurrentLatLon() {
        Criteria cri = new Criteria();
        String provider = locationManager.getBestProvider(cri, false);
        // can replace with argument with LocationManager.GPS_PROVIDER
        Location loc = locationManager.getLastKnownLocation(provider);
        Log.i("karl", "getCurrentLatLon lat " + loc.getLatitude() + " lon " + loc.getLongitude());

        double lon = loc.getLongitude();
        double lat = loc.getLatitude();
        List latLon = new ArrayList<Double>();
        latLon.add(0, lat);
        latLon.add(1, lon);
        return latLon;
    }

    public double getAltitude() {
        Criteria cri = new Criteria();
        String provider = locationManager.getBestProvider(cri, false);
        Location loc = locationManager.getLastKnownLocation(provider);
        return loc.getAltitude();
    }

    class FelaLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location loc) {
            Log.i("karl", "onLocationChanged lat " + loc.getLatitude() + " lon " + loc.getLongitude());
            // only needed for one fix, remove it after
            locationManager.removeUpdates(locationListener);
//        /*------- To get city name from coordinates -------- */
//            String cityName = null;
//            Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
//            List<Address> addresses;
//            try {
//                addresses = gcd.getFromLocation(loc.getLatitude(),
//                        loc.getLongitude(), 1);
//                if (addresses.size() > 0)
//                    System.out.println(addresses.get(0).getLocality());
//                cityName = addresses.get(0).getLocality();
//            }
//            catch (IOException e) {
//                e.printStackTrace();
//            }
//            String s = longitude + "\n" + latitude + "\n\nMy Current City is: "
//                    + cityName;
//            editLocation.setText(s);
        }

        @Override
        public void onProviderDisabled(String provider) {}

        @Override
        public void onProviderEnabled(String provider) {}

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    }

    /**
     * makes a toast on ui thread via runOnUiThread
     * @param message
     * @param type
     */
    public void toast(final String message, String type) {
        int duration = -1;
        if(type.equals("long"))
            duration = Toast.LENGTH_LONG;
        else if (type.equals("short"))
            duration = Toast.LENGTH_SHORT;
        final int finalDuration = Toast.LENGTH_SHORT;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast toast = Toast.makeText(getActivity(), message, finalDuration);
                toast.show();
            }
        });
    }

    /**
     * @author fabien
     * converts latitude and longitude coordinates into exif friendly strings
     */
    public class GPS {
        private StringBuilder sb = new StringBuilder(20);

        /**
         * returns ref for latitude which is S or N.
         * @param latitude
         * @return S or N
         */
        public String latitudeRef(double latitude) {
            return latitude<0.0d?"S":"N";
        }

        /**
         * returns ref for latitude which is S or N.
         * @param latitude
         * @return S or N
         */
        public String longitudeRef(double longitude) {
            return longitude<0.0d?"W":"E";
        }

        /**
         * convert latitude into DMS (degree minute second) format. For instance<br/>
         * -79.948862 becomes<br/>
         *  79/1,56/1,55903/1000<br/>
         * It works for latitude and longitude<br/>
         * @param latitude could be longitude.
         * @return
         */
        synchronized public final String convert(double latitude) {
            latitude=Math.abs(latitude);
            int degree = (int) latitude;
            latitude *= 60;
            latitude -= (degree * 60.0d);
            int minute = (int) latitude;
            latitude *= 60;
            latitude -= (minute * 60.0d);
            int second = (int) (latitude*1000.0d);

            sb.setLength(0);
            sb.append(degree);
            sb.append("/1,");
            sb.append(minute);
            sb.append("/1,");
            sb.append(second);
            sb.append("/1000,");
            return sb.toString();
        }
    }
}