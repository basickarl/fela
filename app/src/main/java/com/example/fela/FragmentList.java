package com.example.fela;


import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by karl on 2015-05-22.
 */
public class FragmentList extends Fragment{

    private Button btnSendReport;

    //Take picture
    private static final int TAKE_PICTURE = 1;
    private File image;
    private Uri capturedImageUri = null;
    private String userEmail;//the email used in phone, to verify user in backend
    //List of Reports
    ListView items_list;
    ArrayList<Report> reports_list = new ArrayList<Report>();
    ReportsAdapter adapter;

    //Database
    private Database db;

    //Test
    TCPClient mTcpClient;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        initComponents(view);
        initListeners();
//        recieveIntent();
        return view;
    }

    private void initComponents(View view) {
        btnSendReport = (Button) view.findViewById(R.id.btn_send_report);
        items_list = (ListView) view.findViewById(R.id.itemsList);
        adapter = new ReportsAdapter(getActivity(), reports_list);
        items_list.setAdapter(adapter);
        db = new Database(getActivity());
        getUserEmail();
        getWifiSSID();
        new connectTask().execute("");
    }

    private void initListeners() {

        btnSendReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Gör inaktiv om ingen är klickad
                int clickedPos = adapter.getSelectedPos();
                if (clickedPos != -1) {
                    Report report = reports_list.get(clickedPos);
                    sendReport(report);
                } else {
                    Toast.makeText(getActivity(), "Nothing is selected..", Toast.LENGTH_SHORT).show();
                }

            }
        });
        items_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapter.setSelectedPosition(position);
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void getUserEmail(){
        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
        Account[] accounts = AccountManager.get(getActivity()).getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                String possibleEmail = account.name;
                userEmail = possibleEmail;
//                Toast.makeText(getActivity(), "email: " + possibleEmail, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void sendReport(Report report){
        //kolla objektets alla delar
        //skicka httpPOST/GET
        //Bekräfta att det tagits emot i vyn som "skickad"

        //Bara För att testa;--------------------------------
        if(adapter.getSelectedPos() > -1){
            if((report.getLat() != 0.0) && (report.getLng() != 0.0)){
                Toast.makeText(getActivity(), "Lat: " + report.getLat() + "Lng: " + report.getLng() + " Alt: " + report.getAltitude(), Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(getActivity(), "Position kunde inte hittas", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void addNewReportToDB(Report r){
        r.setId(db.addReport(r));

        r.setUser(r.getUser());
        r.setLocationName(r.getLocationName());
        r.setLng(r.getLng());
        r.setLat(r.getLat());
        r.setAltitude(r.getAltitude());
        r.setDescription(r.getDescription());
        r.setId(r.getId());
        r.setStatus(r.getStatus());
        r.setPicture(r.getPicture());
    }

    public void addNewReportToView(Report report){
        reports_list.add(report);
        adapter.setSelectedPosition(-1);
        adapter.notifyDataSetChanged();
    }

    private void takePicture() {
        Intent takePictureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {

            image = null;
            try {
                image = createImageFile();
            } catch (IOException ex) {
                Log.i("Fela", "fel på skapandet av filen" + ex.toString());
            }
            // Continue only if the File was successfully created
            if (image != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, capturedImageUri);

                startActivityForResult(takePictureIntent, TAKE_PICTURE);
                Log.i("Fela", "luuugnt");
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_.jpg";
        File image = new File(Environment.getExternalStorageDirectory(), imageFileName);

        if(!image.exists()){
            try {
                image.createNewFile();
                //pathToFile = image.getAbsolutePath();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }else{
            image.delete();
            try {
                image.createNewFile();
                //pathToFile = image.getAbsolutePath();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        capturedImageUri = Uri.fromFile(image);
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {//när kameran är avslutad
        super.onActivityResult(requestCode, resultCode, intent);

        Bitmap bitmap = null;

        if(resultCode != Activity.RESULT_CANCELED){


            if (requestCode == TAKE_PICTURE && resultCode == Activity.RESULT_OK){

                try {
                    bitmap = MediaStore.Images.Media.getBitmap( getActivity().getApplicationContext().getContentResolver(),  capturedImageUri);

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 10, bos);
                    //bitmap.recycle();

                    bos.flush();
                    bos.close();

                    bos = null;

                } catch (IOException e) {
                    e.printStackTrace();
                }

                Log.i("fela", "Sökväg: " + capturedImageUri.getPath());
            }
        }
        Report report = getExifTags(bitmap);
        if(report != null){
            //addNewReportToDB(report);
            confirmSendReport(report);
            JSONHelper j = new JSONHelper();



            //sends message
            if (mTcpClient != null) {
                mTcpClient.sendMessage(j.sendReport(report));
            }

            String s = getWifiSSID();
            Log.i("fela", "SSID:" + s);
        }

    }

    Report temp;

    private void confirmSendReport(final Report report){
        //addNewReportToDB(report);//lägg till vi .positive answer, anropa också "sendreport()"
        temp = report;

        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_layout);
        dialog.setTitle("Vill du skicka denna bild?");

        // set the custom dialog components - text, image and button
        final EditText descriptionText = (EditText) dialog.findViewById(R.id.dialog_description);
        ImageView image = (ImageView) dialog.findViewById(R.id.dialog_image);
        Button ok_button = (Button) dialog.findViewById(R.id.dialog_button_confirm);
        Button cancel_button = (Button) dialog.findViewById(R.id.dialog_button_cancel);

        if(!report.getPicture().matches("")){
            String pictureString = report.getPicture();
            byte[] b = getBytes(pictureString);
            image.setImageBitmap(getBitmap(b));
        }

//        // if button is clicked, close the custom dialog
//        ok_button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if(!"".matches(String.valueOf(descriptionText.getText()).trim())){
//                    temp.setDescription(descriptionText.getText().toString());
//                    addNewReportToDB(temp);
//                }else{
//                    addNewReportToDB(temp);
//                }
//
//
//                dialog.dismiss();
//            }
//        });
//
//        cancel_button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//
//        dialog.show();

    }
//    public Uri getImageUri(Context inContext, Bitmap inImage) {
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//        String path = Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
//        return Uri.parse(path);
//    }

    private byte[] getBytes(String str){
        String[] arr = str.split(",");
        byte[] b = new byte[arr.length];

        for (int i = 0; i < arr.length; i++) {
            b[i] = Byte.parseByte(arr[i]);
        }
        return b;
    }

    private Bitmap getBitmap(byte[] b){
        ByteArrayInputStream imageStream = new ByteArrayInputStream(b);
        Bitmap image = BitmapFactory.decodeStream(imageStream);
        Log.i("fela", "gjort om till bild från string");
        return image;
    }

    private String getWifiSSID(){
        WifiManager manager = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
        if (manager.isWifiEnabled()) {
            WifiInfo wifiInfo = manager.getConnectionInfo();
            List<ScanResult> all = manager.getScanResults();
            if(all != null){

                Iterator<ScanResult> iterator = all.iterator();
                while(iterator.hasNext()){
                    ScanResult sr =  iterator.next();
                    String ssid = sr.SSID;
                    String signal = "" + sr.level;
                    Log.i("fela", "SSID och skit: " + ssid + " " + signal);
                }

            }

            if (wifiInfo != null) {
                NetworkInfo.DetailedState state = WifiInfo.getDetailedStateOf(wifiInfo.getSupplicantState());
                if (state == NetworkInfo.DetailedState.CONNECTED || state == NetworkInfo.DetailedState.OBTAINING_IPADDR) {
                    return wifiInfo.getSSID();
                }
            }
        }
        return "";

    }

    private String getLocationName(double lat, double lng){
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        List<Address> addresses = null;
        try {
            if(lat > 360) // bug in oneplus one device
                lat = 54;
            addresses = geocoder.getFromLocation(lat, lng, 1);
//            String cityName = addresses.get(0).getAddressLine(0);
//            String stateName = addresses.get(0).getAddressLine(1);
//            String countryName = addresses.get(0).getAddressLine(2);
//            return cityName + " " + stateName + " " + countryName;


            String cityName = addresses.get(0).getLocality();
            String stateName = addresses.get(0).getThoroughfare();
            String countryName = addresses.get(0).getCountryName();


            return stateName + " " + cityName + " " + countryName;
        } catch (IOException e) {
            e.printStackTrace();
            return "oh no..";
        }


    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getActivity(), contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void send(String str, Bitmap image, File file) {
        capturedImageUri = Uri.fromFile(file);
        image = sexy(capturedImageUri);

        if(file == null)
            Log.i("karl", "file is null");

        Log.i("karl", "path "+file.getAbsolutePath());


        if(capturedImageUri == null)
            Log.i("karl", "capturedImageUri is null");

        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 10, bos);
            //bitmap.recycle();

            bos.flush();
            bos.close();

            bos = null;

        } catch (IOException e) {
            e.printStackTrace();
        }


        if(image == null)
            Log.i("karl", "image null");
        Report report = getExifTags(image);
        if(!"".matches(String.valueOf(str).trim())){
            if(report == null)
                Log.i("karl", "report null");
            if(str == null)
                Log.i("karl", "str null");


            report.setDescription(str);
            addNewReportToDB(report);
            addNewReportToView(report);
        } else {
            addNewReportToDB(report);
            addNewReportToView(report);
        }





        Response.Listener respListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("karl", response.toString());
            }
        };
        Response.ErrorListener errListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.i("karl", "timeout error / no connection error");
                } else if (error instanceof AuthFailureError) {
                    Log.i("karl", "auth failiure error");
                    //TODO
                } else if (error instanceof ServerError) {
                    Log.i("karl", "server error");
                    //TODO
                } else if (error instanceof NetworkError) {
                    Log.i("karl", "network error");
                    //TODO
                } else if (error instanceof ParseError) {
                    Log.i("karl", "parse error");
                    //TODO
                }
            }
        };
//        String url = "http://192.168.43.64:8070";
        String url = "http://fela.basickarl.io";
        Map<String, String> params = new HashMap<String, String>();
        params.put("resp", JSONHelper.sendReport(report));
//        params.put("resp", "{\"boo1\":\"hee\",\"boo2\":\"hee\",\"boo3\":\"hee\"}");
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        CustomRequest request =
                new CustomRequest(
                        Method.POST,
                        url,
                        params,
                        respListener,
                        errListener);
        requestQueue.add(request);



    }

    public Bitmap sexy(Uri theUri) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 4;

        AssetFileDescriptor fileDescriptor =null;
        try {
            fileDescriptor = getActivity().getContentResolver().openAssetFileDescriptor( theUri, "r");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Bitmap actuallyUsableBitmap
                = BitmapFactory.decodeFileDescriptor(
                fileDescriptor.getFileDescriptor(), null, options);

        Log.i("karl", "'4-sample' method bitmap ... "
                +actuallyUsableBitmap.getWidth() +" "
                +actuallyUsableBitmap.getHeight() );
        return actuallyUsableBitmap;
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    private Report getExifTags(Bitmap bitmap){
        if(bitmap == null)
            Log.i("karl", "bitmap null");

        ExifInterface exif;
        Report report = null;
        Date date=null;
        Bitmap picture = ThumbnailUtils.extractThumbnail(bitmap, 450, 300);
        Double lat = 0.0, lng = 0.0, alt = 0.0;


        try {

            exif = new ExifInterface(capturedImageUri.getPath());

            //Report-date-time
            String string_date = getExifTag(exif,ExifInterface.TAG_DATETIME);
            SimpleDateFormat format = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");

            try {
                date = format.parse(string_date);
            } catch (ParseException e) {
                e.printStackTrace();
                Log.i("fela", "Fel på Date: " + e.toString());
            }

            //Report-latLng:
            float[] latLng = new float[2];
            if(exif.getLatLong(latLng)){
                String latToString = ""+latLng[0];
                String lngToString = ""+latLng[1];
                Log.i("karl", "lat "+latLng[0]);
                Log.i("karl", "lng " + latLng[1]);
                lat = Double.parseDouble(latToString);
                lng = Double.parseDouble(lngToString);
            }

            //Report-Altitude
            alt = exif.getAltitude(-1);



            if(date == null)
                Log.i("karl", "date null");

            if(picture == null)
                Log.i("karl", "picture null");

            //Create Report
            if(picture != null){
                if(date != null){
                    Log.i("karl", "IN HERERE");
                    String imgString = bitmapToString(picture);//bara testa, sen kör
                    report = new Report(imgString, date);
                    report.setUser(userEmail);
                    report.setLat(lat);
                    report.setLng(lng);
                    report.setLocationName(getLocationName(lat, lng));
                    report.setAltitude(alt);
                    report.setStatus(Report.STATUS_NOT_DONE);
                    report.setDescription(Report.DESCRIPTION_NOT_SET);
                }else{
                    Log.i("fela", "date är NULL");
                }

            }else{
                Log.i("fela", "picture är NULL");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return report;
    }

    private String getExifTag(ExifInterface exif,String tag){
        String attribute = exif.getAttribute(tag);
        return (null != attribute ? attribute : "N/A");
    }

    private void recieveIntent(){
        Intent incomingImage = getActivity().getIntent();
        String intentAction = incomingImage.getAction();
        String incomingType = incomingImage.getType();
        //make sure it's an action and type we can handle
        if(intentAction.equals(Intent.ACTION_SEND)){
            if(incomingType.startsWith("image/")){
                //EXTRA_STREAM används för saker förutom text
                Uri receivedUri = (Uri)incomingImage.getParcelableExtra(Intent.EXTRA_STREAM);
                Log.i("fela", "URI: " + receivedUri);
                if (receivedUri != null) {
                    //set the picture
                    //RESAMPLE YOUR IMAGE DATA BEFORE DISPLAYING
                    //picView.setImageURI(receivedUri);//just for demonstration
                    receiveImage(receivedUri);

                }


            }
        }
        else if(intentAction.equals(Intent.ACTION_MAIN)){
            //app has been launched directly, not from share list
            //inget har "delats med.." och appen har startats normalt
            Log.i("fela", "startats direkt");
        }
    }

    public void receiveImage(Uri uri){

        try {
            Log.i("fela", "11111");
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(),uri);
            //Log.i("fela", "2");
            //ByteArrayOutputStream bos = new ByteArrayOutputStream();//hit kommer bilden efter compress
            //Log.i("fela", "3");

            Bitmap bitmapThumb = ThumbnailUtils.extractThumbnail(bitmap, 450, 300);

//			imageContainer.setImageBitmap(bitmapThumb);
//			imageContainer.setVisibility(View.VISIBLE);
//			imageContainer.invalidate();

            //TEST: Skriva bitmap till en annan fil för att hitta rätt sökväg---------

            File horse = createImageFile();
            FileOutputStream fOut = new FileOutputStream(horse);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
            fOut.flush();
            fOut.close();


            //MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), capturedImageUri.getPath(), horse.getName(), horse.getName());

//            printExifTags();//saknar parameter!!
            Log.i("fela", "FIlens - URI: " + capturedImageUri.getPath());
            //----------------------//TEST---------------------------------------------
            //bitmapToString(bitmap);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }



    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    private String bitmapToString(Bitmap bitmap) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        //bitmap.recycle();

        String imgStr = "";

        byte[] bArray = bos.toByteArray();

        try {
            bos.flush();
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        bos = null;
        imgStr = convertToString(bArray);
        bArray = null;

//        ByteBuffer byteBuffer = ByteBuffer.allocate(bitmap.getByteCount());
//        bitmap.copyPixelsToBuffer(byteBuffer);
//        byte[] bytes = byteBuffer.array();
//        String bitmapString = convertToString(bytes);


        Log.i("fela", "Bilden i String: " + imgStr);
        Toast.makeText(getActivity(), "Funkade", Toast.LENGTH_SHORT).show();

        return imgStr;
    }

    private String convertToString(byte[] bytes){
        StringBuilder builder = new StringBuilder();

        for (byte i : bytes) {
            builder.append(i + "," );
        }

        return builder.toString();
    }


    /**
     *  Receives messages in the background in an asynchronous way.
     *
     */
    public class connectTask extends AsyncTask<String,String,TCPClient> {

        //what happens when message is recieved
        @Override					//arbitrary number of elements as parameter
        protected TCPClient doInBackground(String... message) {

            //TCPClient to receive messages
            mTcpClient = new TCPClient(new TCPClient.OnMessageReceived() {
                @Override
                //printing message
                public void messageReceived(String message) {
                    publishProgress(message);
                }
            });
            mTcpClient.run();

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);

        }
    }
}