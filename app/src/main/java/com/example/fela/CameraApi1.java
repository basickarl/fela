package com.example.fela;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.ErrorCallback;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by karl on 2015-05-13.
 */
public class CameraApi1 {

    private Activity activity;
    protected Preview preview = null;
    protected WorkerThread thread = null;
    private int cameraId;
    private Camera camera;
    private PictureCallback pictureCallback;

    public CameraApi1(Activity activity) {
        this.activity = activity;
    }

    /**
     * creates a new preview on a non main thread
     * @param cameraId
     */
    public Preview getPreviewInstance(int cameraId) {
        this.cameraId = cameraId;
        if(thread == null)
            thread = new WorkerThread("camera_thread");
        preview = new Preview();
        return preview;
    }

    public class Preview extends SurfaceView implements SurfaceHolder.Callback {
        private SurfaceHolder holder;
        private boolean previewIsRunning = false;
        private int width = -1;
        private int height = -1;

        public Preview() {
            super(activity.getApplicationContext());
            Log.i("camera_api1", "Preview.Preview(..) thread: " + Thread.currentThread().getName() + " id: " + Thread.currentThread().getId());
            holder = getHolder();
            holder.addCallback(this);
            // deprecated setting, but required on Android versions prior to 3.0
            holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            Log.i("camera_api1", "Preview.surfaceCreated(..) thread: " + Thread.currentThread().getName() + " id: " + Thread.currentThread().getId());
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            Log.i("camera_api1", "Preview.surfaceChanged(..) thread: " + Thread.currentThread().getName() + " id: " + Thread.currentThread().getId());
            if(holder.getSurface() != null)
                thread.surfaceChanged(holder, format, width, height);
        }

        public void surfaceChangedNewThread(SurfaceHolder holder, int format, int width, int height) {
            Log.i("camera_api1", "Preview.surfaceChangedNewThread(..) thread: " + Thread.currentThread().getName() + " id: " + Thread.currentThread().getId());
            this.width = width;
            this.height = height;
            openCamera();

            try {
                Log.i("camera_api1", "camera.setPreviewDisplay(..) thread: " + Thread.currentThread().getName() + " id: " + Thread.currentThread().getId());
                camera.setPreviewDisplay(holder);
            } catch(Exception e) {

            }

            startPreview();
            refresh();
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            thread.releaseCamera();
        }

        synchronized public void startPreview() {
            Log.i("camera_api1", "Preview.startPreview() thread: " + Thread.currentThread().getName() + " id: " + Thread.currentThread().getId());
            openCamera();
            if (!previewIsRunning && (camera != null)) {
                Log.i("camera_api1", "camera.startPreview() thread: " + Thread.currentThread().getName() + " id: " + Thread.currentThread().getId());
                camera.startPreview();
                previewIsRunning = true;
            }
        }

        synchronized public void openCamera() {
            Log.i("camera_api1", "Preview.openCamera() thread: " + Thread.currentThread().getName() + " id: " + Thread.currentThread().getId());
            if (camera == null) {
                camera = getCameraInstance();
                // needed due to onResume() not updating rotation/size
                refresh();
                // needed due to onPause()/onResume() bug (preview not starting)
                if(holder != null) {
                    try {
                        camera.setPreviewDisplay(holder);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        synchronized public void releaseCamera() {
            Log.i("camera_api1", "Preview.releaseCamera() thread: " + Thread.currentThread().getName() + " id: " + Thread.currentThread().getId());
            stopPreview();
            if (camera != null) {
                Log.i("camera_api1", "camera.release() thread: " + Thread.currentThread().getName() + " id: " + Thread.currentThread().getId());
                // needed for onPause()/onResume() bug
                camera.lock();
                camera.release();
                camera = null;
            }
        }

        synchronized public void stopPreview() {
            Log.i("camera_api1", "Preview.stopPreview() thread: " + Thread.currentThread().getName() + " id: " + Thread.currentThread().getId());
            if (previewIsRunning && (camera != null)) {
                Log.i("camera_api1", "camera.stopPreview() thread: " + Thread.currentThread().getName() + " id: " + Thread.currentThread().getId());
                camera.stopPreview();
                previewIsRunning = false;
            }
        }

        public void refresh() {
            if(width == -1 || height == -1)
                return;

            int rotation = ((WindowManager) activity.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
            int degrees = 0;
            // specifically for back facing camera
            switch (rotation) {
                case Surface.ROTATION_0:
                    degrees = 90;
                    break;
                case Surface.ROTATION_90:
                    degrees = 0;
                    break;
                case Surface.ROTATION_180:
                    degrees = 270;
                    break;
                case Surface.ROTATION_270:
                    degrees = 180;
                    break;
            }



            // set optimal preview size depending on the device camera
            Camera.Parameters p = camera.getParameters();
            List<Size> supportedPreviewSizes = p.getSupportedPreviewSizes();
            Size previewSize = getOptimalPreviewSize(supportedPreviewSizes, width, height);
            p.setPreviewSize(previewSize.width, previewSize.height);
            camera.setParameters(p);

            camera.setDisplayOrientation(degrees);
        }
    }

    public Camera getCameraInstance() {
        Log.i("camera_api1", "getCameraInstance() thread: " + Thread.currentThread().getName() + " id: " + Thread.currentThread().getId());
        try {
            camera = Camera.open(cameraId);
        } catch (RuntimeException e) {
            Log.e(null, "failed to open camera");
        }
        // parameters for camera
        Parameters p = camera.getParameters();
//        Camera.Parameters p = camera.getParameters();
//        p.setPictureSize(80, 60);
//        p.setColorEffect(android.hardware.Camera.Parameters.EFFECT_NONE);
//        p.setJpegQuality(20);
//        p.setPreviewFrameRate(1);
//        p.setPreviewFpsRange(5, 10);
//        p.setPreviewSize(80, 60);
//        camera.setParameters(p);
//        p.set("jpeg-quality", 100);
//        p.set("iso", "auto");
//        p.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
//        p.setPictureFormat(PixelFormat.JPEG);
        // set the "take picture" dimensions (not preview)
        List<Size> sizes = p.getSupportedPictureSizes();
        int max = 0, width = 0, height = 0;
        for (Size size : sizes) {
            if (max < (size.width * size.height)) {
                max = (size.width * size.height);
                width = size.width;
                height = size.height;
            }
        }
        p.setPictureSize(width, height);
        camera.setParameters(p);
        // primarily used to fix Error 100
        camera.setErrorCallback(new ErrorCallback() {
            @Override
            public void onError(int error, Camera camera) {
                if (error == Camera.CAMERA_ERROR_SERVER_DIED) {
                    thread.releaseCamera();
                    thread.startPreview();
                }
            }
        });
        return camera;
    }

    private class WorkerThread extends HandlerThread {
        private Handler handler;
        private boolean startingPreview = false;
        private boolean releasingCamera = false;

        public WorkerThread(String name) {
            super(name);
            start();
            handler = new Handler(getLooper());
        }

        public void surfaceChanged(final SurfaceHolder holder, final int format, final int width, final int height) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    preview.surfaceChangedNewThread(holder, format, width, height);
                }
            });
        }

//        public void openCamera() {
//            handler.post(new Runnable() {
//                @Override
//                public void run() {
//                    preview.openCamera();
//                }
//            });
//        }

        public void startPreview() {
            if(!isStartingPreview()) {
                setStartingPreview(true);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        preview.startPreview();
                        setStartingPreview(false);
                    }
                });
            }
        }

//        public void stopPreview() {
//            if(preview != null) {
//                handler.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        preview.stopPreview();
//                    }
//                });
//            }
//        }

        public void releaseCamera() {
            if (!isReleasingCamera()) {
                setReleasingCamera(true);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        preview.releaseCamera();
                        setReleasingCamera(false);
                    }
                });
            }
        }

//        public void takePic() {
//            handler.post(new Runnable() {
//                @Override
//                public void run() {
//                    camera.takePicture(null, null, pictureCallback);
//                    startPreview();
//                }
//            });
//        }

        synchronized public void setReleasingCamera(boolean releasingCamera) {
            this.releasingCamera = releasingCamera;
        }

        synchronized public void setStartingPreview(boolean startingPreview) {
            this.startingPreview = startingPreview;
        }

        synchronized public boolean isStartingPreview() {
            return startingPreview;
        }

        synchronized public boolean isReleasingCamera() {
            return releasingCamera;
        }
    }

    public void startPreview() {
        thread.startPreview();
    }

    public void releaseCamera() {
        thread.releaseCamera();
    }

    public void takePic() {
        camera.takePicture(null, null, pictureCallback);
    }

    public void quit() {
        thread.quit();
        preview = null;
    }

//    public void onResume(LinearLayout previewLayout) {
//        previewLayout.removeAllViews();
//        SurfaceView surfaceView = new SurfaceView(previewLayout.getContext());
//        previewLayout.addView(surfaceView, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//
//        SurfaceHolder previewHolder = surfaceView.getHolder();
//        previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
//        previewHolder.addCallback(surfaceHolderCallback);
//    }

    /**
     * checking device has camera hardware or not
     * */
    public boolean hasCamera() {
        // if this device has a camera
        if (activity.getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA))
            return true;
        // this device does not have a camera
        return false;
    }

    /**
     * makes the image visible for the device (gallery)
     * @param file
     */
    public void galleryAddPic(File file) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        activity.sendBroadcast(mediaScanIntent);
    }

    /**
     * get back camera facing id (non google play services approach)
     * @return camera id (-1 if not found)
     */
    public int getCameraIdFacingBack() {
        int cameraId = -1;
        // get the number of cameras
        int numberOfCameras = Camera.getNumberOfCameras();
        // check all cameras
        for (int i = 0; i < numberOfCameras; i++) {
            CameraInfo info = new CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

    private Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio=(double)h / w;

        if (sizes == null) return null;

        Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    // make picture and save to a directory
    public File getOutputMediaFile(String directory) {
        // make a new file directory inside the "sdcard" folder
        // File mediaStorageDir = new File("/sdcard/", "fela"); // private pic for app
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), directory);

        // if directory does not exist
        if (!mediaStorageDir.exists()) {
            // if you cannot make this directory return
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        // take the current timeStamp
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        // and make a media file:
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }

    public void setPictureCallback(PictureCallback pictureCallback) {
        this.pictureCallback = pictureCallback;
    }

}
