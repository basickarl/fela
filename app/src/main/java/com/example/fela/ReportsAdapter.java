package com.example.fela;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by ronny on 2015-04-10.
 */
public class ReportsAdapter extends BaseAdapter {
    private Activity context;
    private List<Report> list;
    private LayoutInflater inflater;
    private int selectedPosition = -1;


    public ReportsAdapter(Activity context, List<Report> list){
        this.context = context;
        this.list = list;
        inflater = context.getLayoutInflater();
    }

    public void setSelectedPosition(int selectedPosition){
        this.selectedPosition = selectedPosition;
    }

    public int getSelectedPos(){
        return selectedPosition;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getId();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;

        //create new view-item(list row) or recycle
        if(view == null){
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.list_item, null);

            holder.picture = (ImageView) view.findViewById(R.id.report_list_image);
            holder.locationName = (TextView) view.findViewById(R.id.report_list_locationName);
            holder.date = (TextView) view.findViewById(R.id.report_list_date);
            view.setTag(holder);
        }else{
            holder = (ViewHolder) view.getTag();
        }

        //fetch object-attributes from list
        String pictureString = list.get(position).getPicture();
        String locationName = list.get(position).getLocationName();
        Date date = list.get(position).getDate();

        //add attributes to viewed-item
        if(!pictureString.matches("")){
            byte[] b = getBytes(pictureString);
            //getBitmap(b)<------i metodanropet
            holder.picture.setImageBitmap(getBitmap(b));
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");

        holder.date.setText(dateFormat.format(date));
        holder.locationName.setText(locationName);

        //highlight touched position
        if(position==selectedPosition){
            view.setBackgroundColor(Color.MAGENTA);
        }else{
            view.setBackgroundColor(Color.WHITE);
        }

        return view;
    }

    public static class ViewHolder{
        ImageView picture;
        TextView locationName;
        TextView date;
    }

    private byte[] getBytes(String str){
        String[] arr = str.split(",");
        byte[] b = new byte[arr.length];

        for (int i = 0; i < arr.length; i++) {
            b[i] = Byte.parseByte(arr[i]);
        }
        return b;
    }

    private Bitmap getBitmap(byte[] b){
        ByteArrayInputStream imageStream = new ByteArrayInputStream(b);
        Bitmap image = BitmapFactory.decodeStream(imageStream);
        return image;
    }
}
