package com.example.fela;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ronny on 2015-04-16.
 */
public class Database {

    private SQLiteDatabase database;
    private DatabaseHelper helper;
    private MainActivity activity;
    private FragmentList fragment;

    public Database(Activity activity){
        fragment = ((MainActivity) activity).getFragmentList();
        this.activity = (MainActivity) activity;
        helper = new DatabaseHelper(activity, DatabaseHelper.DATABASE);

        readAllReports();
    }

    public void readAllReports(){
        open();
        Cursor c = getReports();
        SimpleDateFormat format = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
        Date date = null;
        if( c.moveToFirst() ){
            do{
                String str_date = c.getString(5).toString();


                try {
                    date = format.parse(str_date);
                } catch (ParseException e) {
                    e.printStackTrace();
                    Log.i("fela", "Fel på Date: " + e.toString());
                }


                Report report = new Report(c.getString(1), date);//image och date
                report.setId(c.getInt(0));
                report.setStatus(c.getString(2));
                report.setLat(c.getDouble(3));
                report.setLng(c.getDouble(4));
                report.setLocationName(c.getString(6));
                report.setUser(c.getString(7));
                report.setAltitude(c.getDouble(8));
                report.setDescription(c.getString(9));

                if (fragment == null){
                    Log.i("fela", "fragment är null.....");
                }else
                    Log.i("fela", "fragment är INTE null.....");
                //report.setPic(getBitmap(getBytes(report.getPicture())));
                fragment.addNewReportToView(report);


            }while(c.moveToNext());
        }
        close();
    }

   public int addReport(Report report){
       SimpleDateFormat format = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
       String str_date = format.format(report.getDate());

       ContentValues vals = new ContentValues();
       vals.put(helper.REPORT_IMAGE, report.getPicture());
       vals.put(helper.REPORT_DATE_OF_ISSUE, str_date);
       vals.put(helper.REPORT_LATITUDE, report.getLat());
       vals.put(helper.REPORT_LONGITUDE, report.getLng());
       vals.put(helper.REPORT_STATUS, report.getStatus());
       vals.put(helper.REPORT_USER_EMAIL, report.getUser());
       vals.put(helper.REPORT_ALTITUDE, report.getAltitude());
       vals.put(helper.REPORT_DESCRIPTION, report.getDescription());
       vals.put(helper.REPORT_LOCATION_NAME, report.getLocationName());
       open();
       database.insert(helper.TABLE_REPORT, null, vals);

       Cursor c = getReports();
       c.moveToLast();
       int reportID = c.getInt(0);
       close();
       return reportID;
    }

    private byte[] getBytes(String str){
        String[] arr = str.split(",");
        byte[] b = new byte[arr.length];

        for (int i = 0; i < arr.length; i++) {
            b[i] = Byte.parseByte(arr[i]);
        }
        return b;
    }

    private Bitmap getBitmap(byte[] b){
        ByteArrayInputStream imageStream = new ByteArrayInputStream(b);
        Bitmap image = BitmapFactory.decodeStream(imageStream);
        return image;
    }

//    public void updateReport(Report report) {
//        ContentValues vals = new ContentValues();
//        vals.put(EXPENSE_TITLE, expense.getTitel());
//        vals.put(EXPENSE_CATEGORY, expense.getCategory());
//        vals.put(EXPENSE_PRICE, expense.getPrice());
//        vals.put(EXPENSE_IMAGE, expense.getImage());
//        vals.put(DATE, expense.getDate());
//        open();
//        database.update(TABLE_EXPENSE, vals, EXPENSE_ID + " = " + expense.getId(), null);
//        Cursor c = getExpenses();
//        c.moveToLast();
//        close();
//    }

//    public void removeIncome(int id){
//        open();
//        database.delete(TABLE_INCOME, INCOME_ID + "=" + id, null);
//        close();
//    }

    public Cursor getReports(){
        Cursor c = database.rawQuery("SELECT * FROM "+helper.TABLE_REPORT, null);
        return c;
    }

    private Database open(){
        database = helper.getWritableDatabase();
        return this;
    }

    private void close(){
        database.close();
    }
//    public void updateExpense(Expense expense) {
//        ContentValues vals = new ContentValues();
//        vals.put(EXPENSE_TITLE, expense.getTitel());
//        vals.put(EXPENSE_CATEGORY, expense.getCategory());
//        vals.put(EXPENSE_PRICE, expense.getPrice());
//        vals.put(EXPENSE_IMAGE, expense.getImage());
//        vals.put(DATE, expense.getDate());
//        open();
//        database.update(TABLE_EXPENSE, vals, EXPENSE_ID + " = " + expense.getId(), null);
//        Cursor c = getExpenses();
//        c.moveToLast();
//        close();
//    }

//    public void removeIncome(int id){
//        open();
//        database.delete(TABLE_INCOME, INCOME_ID + "=" + id, null);
//        close();
//    }
}
