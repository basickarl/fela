package com.example.fela;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.fela.CameraPreview.CameraPreviewActivityInterface;
import com.example.fela.FragmentCamera.FragmentCameraActivityInterface;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.io.File;
import java.util.Locale;

public class MainActivity extends FragmentActivity implements FragmentCameraActivityInterface, CameraPreviewActivityInterface {

	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

	private FragmentMain fragmentMain;
	private FragmentCamera fragmentCamera;
	private FragmentList fragmentList;
	private File pictureTaken;

    private SwipeViewAdapter swipeViewAdapter;
	private ViewPager viewPager;
	private final int DEFAULT_FRAGMENT = 0;
	private int currentPage = DEFAULT_FRAGMENT; // default page

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		swipeViewAdapter = new SwipeViewAdapter(getSupportFragmentManager());
		viewPager = (ViewPager) findViewById(R.id.pager);
		viewPager.setAdapter(swipeViewAdapter);
		//viewPager.setOffscreenPageLimit(3);

		// check availability of google play services (Android version > 2.3)
		if(checkPlayServices()) {

		}

		// handle sharing to this application (example gallery image)
		Intent intent = getIntent();
		String action = intent.getAction();
		String type = intent.getType();
		// if something was shared to this app
		if (Intent.ACTION_SEND.equals(action) && type != null) {
			// if a single image was shared to this app
			if (type.startsWith("image/")) {
				// set the start screen to the fragment which can handle the intent
				viewPager.setCurrentItem(1);
			}
		} else { // if nothing was shared to this app
			// set default start screen
			viewPager.setCurrentItem(currentPage);
		}

		// used to pause and start the camera view when swiping (lags)
		viewPager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
			}

			@Override
			public void onPageSelected(int position) {
//				Log.i(null, "MainActivity onPageSelected(" + position + ")");
				currentPage = position;
				if (currentPage == 0) {
					fragmentCamera.fragmentVisible();
				} else {
					fragmentCamera.fragmentNotVisible();
				}
				if (currentPage == 1) {
					fragmentMain.fragmentVisible();
				} else {
					fragmentMain.fragmentNotVisible();
				}
			}

			@Override
			public void onPageScrollStateChanged(int state) {
				if(state == ViewPager.SCROLL_STATE_IDLE)
					Log.i(null, "SCROLL_STATE_IDLE");
			}
		});
	}

	public void setCurrentPage(int page) {
		viewPager.setCurrentItem(page);
	}

	public void setPictureTaken(File file) {
		pictureTaken = file;
	}

	public File getPictureTaken() {
		return pictureTaken;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private class SwipeViewAdapter extends FragmentPagerAdapter{

		public SwipeViewAdapter(android.support.v4.app.FragmentManager fm) {
			super(fm);
		}

		@Override
		public android.support.v4.app.Fragment getItem(int position) {
			
			switch (position) {
			case 0:
				fragmentCamera = new FragmentCamera();
				return fragmentCamera;
			case 1:
				fragmentMain = new FragmentMain();
				return fragmentMain;
				case 2:
					fragmentList = new FragmentList();
					return fragmentList;
			}
			
			return null;
		}

		@Override
		public int getCount() {
			return 3;
		}
		
		@Override  
        public CharSequence getPageTitle(int position) {  
            Locale l = Locale.getDefault();  
            switch (position) {  
            case 0:
            	return "0";
            case 1:  
            	return "1";
				case 2:
					return "2";
            }  
            return null;  
        }  
	}

	/**
	 * Method to verify google play services on the device
	 * */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Toast.makeText(getApplicationContext(),
						"This device is not supported.", Toast.LENGTH_LONG)
						.show();
				finish();
			}
			return false;
		}
		return true;
	}

	@Override
	public int getCurrentPage() {
		return currentPage;
	}

	public FragmentList getFragmentList(){
		return fragmentList;
	}

	public void send(String str, Bitmap image, File file) {
		fragmentList.send(str, image, file);
	}


}

