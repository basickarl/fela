package com.example.fela;

import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonWriter;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by root on 2015-05-04.
 */
public class JSONHelper {




    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static String sendReport(Report report){
        StringWriter stringWriter = new StringWriter();
        JsonWriter writer = new JsonWriter( stringWriter );
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");

        try {
            writer.beginObject()
                    .name("type")
                    .value("fela")//name for one report to Backend...
                    .name("picture")
                    .value(report.getPicture())
                    .name("id")
                    .value(report.getId())
                    .name("date")
                    .value(dateFormat.format(report.getDate()))
                    .name("latitude")
                    .value("" + report.getLat())
                    .name("longitude")
                    .value("" + report.getLng())
                    .name("altitude")
                    .value("" + report.getAltitude())
                    .name("locationName")
                    .value(report.getLocationName())
                    .name("user")
                    .value(report.getUser())
                    .name("status")
                    .value(report.getStatus())
                    .name("description")
                    .value(report.getDescription())
                    .endObject();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return stringWriter.toString();
    }

    //Inte testad
    private void getAllReports(JSONObject jObj) {
        Double lat = 0.0, lng = 0.0, alt = 0.0;
        int id = -1;
        String picture = "", locationName = "", user = "", description = "", status = "", str_date = "";
        Date date = null;
        JSONArray jArray = null;
        ArrayList<Report> allReports = new ArrayList<Report>();
        Report report;
        try {
            jArray = jObj.getJSONArray("FelaList");
        } catch (JSONException e1) {
            Log.e("fela", "ERROR: " + e1.toString());
        }

        for (int i = 0; i < jArray.length(); i++) {

            try {

                JSONObject fela = jArray.getJSONObject(i);
                picture = fela.getString("picture");
                locationName = fela.getString("locationName");
                id = fela.getInt("id");
                str_date = fela.getString("date");
                lat = fela.getDouble("latitude");
                lng = fela.getDouble("longitude");
                alt = fela.getDouble("altitude");
                user = fela.getString("user");
                status = fela.getString("status");
                description = fela.getString("description");

                SimpleDateFormat format = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
                date = format.parse(str_date);


            } catch (JSONException e) {
                Log.e("fela", "ERROR: " + e.toString());
            } catch (NumberFormatException e2) {
                Log.e("fela", "ERROR: " + e2.toString());
            } catch (ParseException e) {
                e.printStackTrace();
                Log.i("fela", "Fel på Date: " + e.toString());
            }

            report = new Report(picture, date);
            report.setDescription(description);
            report.setStatus(status);
            report.setAltitude(alt);
            report.setId(id);
            report.setLat(lat);
            report.setLng(lng);
            report.setLocationName(locationName);
            report.setUser(user);

            allReports.add(report);
            // TODO Auto-generated catch block
            //Skicka eller return'a arraylisten någonstans
        }
    }


    //tänkte; ["type", "status"] och "status", "reportID"
    public static void getStatus(JSONObject jObj){
        String status = "", reportID = "";

//        JSONArray jArray = null;

        try {
//            jArray = jObj.getJSONArray("FelaStatus");
            status = jObj.getString("status");
            reportID = jObj.getString("id");

        } catch (JSONException e1) {
            Log.e("fela", "ERROR: " + e1.toString());
        }

//        for (int i = 0; i < jArray.length(); i++) {
//            try {
//                JSONObject fela_status = jArray.getJSONObject(i);
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
    }



}
