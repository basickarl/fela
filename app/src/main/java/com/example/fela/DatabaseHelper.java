package com.example.fela;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ronny on 2015-04-16.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    protected static final String DATABASE = "fela_DB.db";
    protected static final int 	VERSION  = 12;


    //Table name
    protected static final String TABLE_REPORT = "Report";
    //Table column names
    protected static final String _id = "ID";
    protected static final String REPORT_IMAGE = "Image";

    protected static final String REPORT_LATITUDE = "Latitude";
    protected static final String REPORT_LONGITUDE = "Longitude";
    protected static final String REPORT_DATE_OF_ISSUE = "Date";

    protected static final String REPORT_STATUS = "Status";

    protected static final String REPORT_LOCATION_NAME = "LocationName";

    protected static final String REPORT_USER_EMAIL = "UserEmail";

    protected static final String REPORT_ALTITUDE = "Altitude";
    protected static final String REPORT_DESCRIPTION = "Description";

//    protected static final String REPORT_COMPANY_INFO = "CInfo";//bort-reviderade
//    protected static final String REPORT_CATEGORY = "Category";//bort-reviderade

    //Create TABLE_REPORT
    private static final String CREATE_REPORT = "create table if not exists " + TABLE_REPORT + "(" +
            _id + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            REPORT_IMAGE         + " TEXT," +
            REPORT_STATUS        + " TEXT," +
            REPORT_LATITUDE      + " REAL," +
            REPORT_LONGITUDE     + " REAL," +
            REPORT_DATE_OF_ISSUE + " TEXT," +
            REPORT_LOCATION_NAME + " TEXT," +
            REPORT_USER_EMAIL    + " TEXT,"  +
            REPORT_ALTITUDE      + " REAL," +
            REPORT_DESCRIPTION   + " TEXT"
            + ");";

    public DatabaseHelper(Context context, String DATABASE){
        super(context, DATABASE, null, VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_REPORT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REPORT);
        onCreate(db);
    }
}
